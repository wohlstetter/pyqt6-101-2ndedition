class myImmortalClass:
    averageSpeed = 5
    averageStrength = 5

    def getLifespan(self):
        return "Immortal"

class myVampireClass(myImmortalClass):
    subSpeciesName = 'Vampire'
    averageSpeed = 15
    averageStrength = 15

    def __init__(self, name):
        self._name = name

    def greeting(self):
        print(f"Hi! My name is {self._name} and I'm a {self.subSpeciesName}. Lycans were made to serve us, Humans to feed us.")

class myLycanClass(myImmortalClass):
    subSpeciesName = 'Lycan'
    averageSpeed = 25
    averageStrength = 25

    def __init__(self, name):
        self._name = name

    def greeting(self):
        print(f"Hi! My name is {self._name} and I'm a {self.subSpeciesName}. No matter what any bat claims, Lycans and Vampires are equals.")

class myMichaelPostLycanBiteClass(myLycanClass):
    def __init__(self):
        pass

    def greeting(self):
        print(f"Hello! My name is Michael Corvin. I'm ~24 and a surgeon's intern. Please don't tell anyone that I was recently bitten by a wolf.")
