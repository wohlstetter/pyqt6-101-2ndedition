class myBaseClass:
    def __init__(self):
        print("myBaseClass.__init__ called")

class myChildClass1(myBaseClass):
    def __init__(self):
        myBaseClass.__init__(self)
        print("myChildClass1.__init__ called")

class myChildClass2(myBaseClass):
    def __init__(self):
        myBaseClass.__init__(self)
        print("myChildClass2.__init__ called")

class myGrandChildClass(myChildClass1, myChildClass2):
    def __init__(self):
        myChildClass1.__init__(self)
        myChildClass2.__init__(self)
        print("myGrandChildClass.__init__ called")
