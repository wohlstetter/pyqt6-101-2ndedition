class myImmortalClass:
    def getLifespan(self):
        return "Immortal"

class myVampireClass(myImmortalClass):
    subSpeciesName = 'Vampire'          # Class variables are generally shared by all instances of a class.

    def __init__(self, name):
        self._name = name

    def greeting(self):
        print(f"Hi! My name is {self._name} and I'm a {self.subSpeciesName}. Lycans were made to serve us, Humans to feed us.")

class myLycanClass(myImmortalClass):
    subSpeciesName = 'Lycan'

    def __init__(self, name):
        print("myLycanClass has been invoked.") # Notice that this method is invoked when a myMichaelPostLycanBiteClass object is created.
        self._name = name

    def greeting(self):
        print(f"Hi! My name is {self._name} and I'm a {self.subSpeciesName}. No matter what any bat claims, Lycans and Vampires are equals.")

class myMichaelPostLycanBiteClass(myLycanClass):

    def salutation(self):
        print(f"Hello! My name is {self._name}. I'm ~28 and a surgeon's intern. Please don't tell anyone that I was recently bitten by a wolf.")
