import requests

CURRENCYLAYER_API_KEY = "put your API key here"

#PS: The request URL may have changed by the time you read this. Check the apilayer page to make sure you're using the latest format!
r = requests.get("https://api.apilayer.com/currency_data/live?source=USD&currencies=AUD%2CCAD%2CXAU%2CBTC", headers={"apikey":CURRENCYLAYER_API_KEY})

response = r.json()

print("%.10f" % response['quotes']['USDBTC'])
