from PyQt6 import QtCore, QtGui, QtWidgets
from MainWindow import Ui_MainWindow
import requests

CURRENCYLAYER_API_KEY = "put your API key here"

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setupUi(self)

        self.convertButton.clicked.connect(self.talkToService)
        
        self.show()

    def talkToService(self):
        r = requests.get("https://api.apilayer.com/currency_data/live?source=USD&currencies=AUD%2CCAD%2CXAU%2CBTC",  headers={"apikey":CURRENCYLAYER_API_KEY})

        response = r.json()

        if r.status_code == 200:
            self.cadLabel.setText(str(response['quotes']['USDCAD']))
            self.audLabel.setText(str(response['quotes']['USDAUD']))
            self.xauLabel.setText(str(response['quotes']['USDXAU']))
            self.btcLabel.setText("%.10f" % response['quotes']['USDBTC'])
        else:
            print(response)
            QtWidgets.QMessageBox.critical(self, "Exception",
                                    "Something went wrong!")
        

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec())
