from PyQt6.QtWidgets import QMessageBox, QApplication, QTableView, QVBoxLayout, QPushButton, QWidget, QHeaderView
from PyQt6.QtSql import QSqlDatabase, QSqlRelationalTableModel, QSqlRelation, QSqlRelationalDelegate
from PyQt6 import QtCore

def initializeModel(model):
    model.setTable('drinks')
    model.EditStrategy.OnRowChange
    model.setHeaderData(1, QtCore.Qt.Orientation.Horizontal, 'Name')
    model.setHeaderData(2, QtCore.Qt.Orientation.Horizontal, 'Type of drink')
    model.setRelation(2, QSqlRelation('category', 'id', 'drinktype'))
    model.select()

def createView(model):
    view = QTableView()
    view.setModel(model)
    view.setItemDelegate(QSqlRelationalDelegate(view))
    view.hideColumn(0)
    view.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Stretch)
    view.verticalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Stretch)
    return view

def addRecord():
    model.insertRow(model.rowCount())
    view.scrollToBottom()
    
def delRecord():
    model.deleteRowFromTable(view.currentIndex().row())
    model.select()

if __name__ == '__main__':
    import sys
    import os

    app = QApplication(sys.argv)
    filename = os.path.join(os.path.dirname(__file__), "pyqt101.db")

    con = QSqlDatabase.addDatabase('QSQLITE')
    con.setDatabaseName(filename)
    if not con.open():
        QMessageBox.critical(None, "Cannot open database",
                             "Unable to establish a database connection.\n",
                             QMessageBox.Cancel)
        sys.exit(1)

    model = QSqlRelationalTableModel()
    initializeModel(model)

    view = createView(model)

    vbox = QVBoxLayout()
    vbox.addWidget(view)
    btnAdd = QPushButton("&Add record")
    btnAdd.clicked.connect(addRecord)
    vbox.addWidget(btnAdd)
    btnDel = QPushButton("&Delete record")
    btnDel.clicked.connect(delRecord)
    vbox.addWidget(btnDel)

    window = QWidget()
    window.setWindowTitle("Database demo 4")
    window.setLayout(vbox)
    window.resize(340, 300)
    window.show()
    sys.exit(app.exec())
